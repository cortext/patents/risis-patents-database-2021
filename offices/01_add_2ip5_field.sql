USE risis_patents_2021_staging;

UPDATE rpd_patent_value
SET `_2ip5` = if((US+EP+JP+CN+KR) >= 2, 1, 0);
-- Query OK, 3567686 rows affected (27,60 sec)
-- Rows matched: 20656894  Changed: 3567686  Warnings: 0