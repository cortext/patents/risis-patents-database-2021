USE risis_patents_2021_staging;

-- Backup table (onyl the fields that will be updated)
CREATE TABLE risis_patents_2021_backups.backup_221118_rpd_patent_value AS
SELECT appln_id, singleton, transnat, ip5_family
FROM rpd_patent_value;
-- Query OK, 20656894 rows affected (12.83 sec)
-- Records: 20656894  Duplicates: 0  Warnings: 0

UPDATE rpd_patent_value
SET singleton = IF(nb_patents_inpadoc = 1, 1, 0);
-- Query OK, 60184 rows affected (5.56 sec)
-- Rows matched: 20656894  Changed: 60184  Warnings: 0

UPDATE rpd_patent_value
SET transnat = IF(nb_offices_inpadoc > 1, 1, 0);
-- Query OK, 824047 rows affected (18.93 sec)
-- Rows matched: 20656894  Changed: 824047  Warnings: 0

UPDATE rpd_patent_value
SET ip5_family = IF(nb_offices_inpadoc > 1 AND EP + US + JP + CN + KR >= 1, 1, 0);
-- Query OK, 1629657 rows affected (35.20 sec)
-- Rows matched: 20656894  Changed: 1629657  Warnings: 0
