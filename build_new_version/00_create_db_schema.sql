-- Create risis patents_2021 databases
CREATE DATABASE IF NOT EXISTS `risis_patents_2021_lab` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
CREATE DATABASE IF NOT EXISTS `risis_patents_2021_main` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
CREATE DATABASE IF NOT EXISTS `risis_patents_2021_staging` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
CREATE DATABASE IF NOT EXISTS `risis_patents_2021_backups` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
