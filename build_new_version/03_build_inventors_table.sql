-- Build new inventors table 
USE risis_patents_2021_staging;

DROP TABLE IF EXISTS rpd_inventors;
-- Query OK, 0 rows affected, 1 warning (0.00 sec)

CREATE TABLE `rpd_inventors` (
    `appln_id` int(10) NOT NULL DEFAULT '0',
    `person_id` int(10) NOT NULL DEFAULT '0',
    `person_name` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
    `adr_final` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
    `iso_ctry` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
    `continent_final` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
    `latitude` double DEFAULT NULL,
    `longitude` double DEFAULT NULL,
    `confidence` float DEFAULT NULL,
    `iso2` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
    `ura_uniqueid` int(8) DEFAULT NULL,
    `rurban_area_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
    `rurban_area_name` varchar(75) COLLATE utf8_unicode_ci DEFAULT NULL,
    `rurban_area_characteristics` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
    `nuts_source` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
    `nuts_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
    `frac_inventor` float DEFAULT NULL,
    `adr_source` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
    PRIMARY KEY (`appln_id`, `person_id`),
    KEY `appln_id_idx` (`appln_id`) USING BTREE,
    KEY `person_id_idx` (`person_id`) USING BTREE,
    KEY `adr_final_idx` (`adr_final`(300)),
    KEY `person_name_idx` (`person_name`) USING BTREE,
    KEY `iso_ctry_idx` (`iso_ctry`) USING BTREE,
    KEY `continent_final_idx` (`continent_final`) USING BTREE,
    KEY `confidence_idx` (`confidence`) USING BTREE,
    KEY `rurban_area_id_idx` (`rurban_area_id`) USING BTREE,
    KEY `rurban_area_name_idx` (`rurban_area_name`) USING BTREE,
    KEY `rurban_area_characteristics_idx` (`rurban_area_characteristics`) USING BTREE,
    KEY `nuts_source_idx` (`nuts_source`) USING BTREE,
    KEY `nuts_id_idx` (`nuts_id`) USING BTREE,
    KEY `frac_inventor_idx` (`frac_inventor`) USING BTREE,
    KEY `lon_lat_idx` (`longitude`, `latitude`) USING BTREE,
    KEY `adr_final_iso_ctry_idx` (`adr_final`(300), `iso_ctry`),
    KEY `adr_source_idx` (`adr_source`),
    KEY `iso2_idx` (`iso2`)
) ENGINE = MYISAM DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Query OK, 0 rows affected (0.00 sec)

ALTER TABLE rpd_inventors DISABLE KEYS;
-- Query OK, 0 rows affected (0.01 sec)

-- Natural person applicants within perimeter
INSERT INTO
    rpd_inventors(
        appln_id,
        person_id,
        person_name,
        adr_final,
        iso_ctry,
        adr_source
    )
SELECT
    inventor.appln_id,
    inventor.person_id,
    inventor.person_name,
    inventor.adr_final,
    inventor.iso_ctry,
    inventor.source
FROM
    risis_patents_2021_lab.rpd_appln_perimeter patent_appln
    INNER JOIN patstat2021_staging.invt_addr_ifris inventor ON patent_appln.appln_id = inventor.appln_id;
-- Query OK, 59150156 rows affected (3 hours 46 min 30.07 sec)
-- Records: 59150156  Duplicates: 0  Warnings: 0

ALTER TABLE rpd_inventors ENABLE KEYS;
-- Query OK, 0 rows affected (55 min 52.74 sec)

-- Add calculation of fractional count
UPDATE
    rpd_inventors inventor
    INNER JOIN (
        SELECT
            appln_id,
            1 / count(*) AS fcount
        FROM
            rpd_inventors
        GROUP BY
            appln_id
    ) frac ON inventor.appln_id = frac.appln_id
SET
    inventor.frac_inventor = frac.fcount;
-- Query OK, 59150156 rows affected, 65535 warnings (54 min 47.31 sec)
-- Rows matched: 59150156  Changed: 59150156  Warnings: 5082763

-- Add longitude, latitude info from the addresses dictionary built in the previous version
UPDATE
    rpd_inventors inventor
    LEFT JOIN patstat2021_lab.addr_ifris addr_dict ON (
        inventor.adr_final = addr_dict.addr_final
        AND inventor.iso_ctry = addr_dict.iso_ctry
    )
SET
    inventor.latitude = addr_dict.latitude,
    inventor.longitude = addr_dict.longitude,
    inventor.confidence = addr_dict.confidence,
    inventor.iso2 = addr_dict.iso2;
-- Query OK, 36714528 rows affected (1 hour 58 min 37.96 sec)
-- Rows matched: 59150156  Changed: 36714528  Warnings: 0

CREATE INDEX idx_ura_uniqueid USING BTREE ON rpd_inventors (ura_uniqueid);
-- Query OK, 59150156 rows affected (1 hour 13 min 32.22 sec)
-- Records: 59150156  Duplicates: 0  Warnings: 0

-- Waiting for geomapping results
-- Add NUTS and URA info from the previous version
UPDATE
    rpd_inventors a
    LEFT JOIN patstat2021_lab.addr_ifris_geo_mapping b ON (
      a.longitude = b.longitude 
      AND a.latitude = b.latitude
    )
SET
    a.ura_uniqueid = b.ura_uniqueid,
    a.rurban_area_id = b.rurban_area_id,
    a.rurban_area_name = b.rurban_area_name,
    a.rurban_area_characteristics = b.rurban_area_characteristics,
    a.nuts_source = b.nuts_source,
    a.nuts_id = b.nuts_id
WHERE a.longitude IS NOT NULL AND a.ura_uniqueid IS NULL;
-- Query OK, 36500331 rows affected (5 hours 29 min 1.98 sec)
-- Rows matched: 36714528  Changed: 36500331  Warnings: 0
