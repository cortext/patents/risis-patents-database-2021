# RISIS Patents database - version 2021

## Build new version

### [Create RPD 2021 databases](00_create_db_schema.sql)
  - Creates databases for:
    - Useful data: `risis_patents_2021_lab`
    - Main DB: `risis_patents_2021_main`, stable and latest version of changes, which is used to provide the patents data in the [RISIS Core Facility](https://rcf.risis2.eu/dataset/5/metadata).
    - Staging DB: `risis_patents_2021_staging`, where all the operations are performed before a big change is included in the Main DB.
    - Backups DB: `risis_patents_2021_backups`

    #### Flow
    - Before any table update in `Staging DB`, a backup table must be created in `Backups DB` with this name format: `backupYYYYMMDD_tablename`.
    - After the needed changes are performed and tested in `Staging DB`, the table can be moved to `Main DB`, replacing the old one there, if any.

### [Filter patents from PATSTAT](01_create_appln_table.sql)
  - Creates a table (`rpd_appln_perimeter`), taking the patents of our interest from the current state of Patstat (`patstat2021_staging`.`tls201_appln`), bringing all the fields but filtering some rows by the following conditions:
    - Priority patents: it means they were the first application for a given patent. Previously the condition was `appln_first_priority_year = 0`, now we have the field `earliest_filing_id` which allows us to directly identify them.
    - Filed after 2000: `appln_filing_year >= 2000`.
    - Excluding year 9999: it is a wildcard, so we exclude the patents with this year value.
    - Only Patent of Invention (PI) type: `ipr_type` field contains these options, which mean:
      - PI - Patent of Invention
      - UM - Utility Model
      - DP - Design Patent
    - A, W or P application kind: `appln_kind` field specify the kind of application:
      - A - patent
      - U - utility model
      - W - PCT application (in the international phase)
      - T - used by some offices (e. g. AT, DE, DK, ES, GR, HR, PL, PT, SI, SM, TR) for applications which are "translations" of granted PCT or EP applications
      - P - provisional application (US only)
      - F - design patent
      - V - plant patent
      - D2, D3 - artificial applications (see section 4.4 "Application replenishment")
      - Based on [Patstat documentation](https://gitlab.com/cortext/patents/patstat-2021/-/blob/main/DataCatalog_Global_v5.17.pdf)

### [Actors table](02_create_actors_table.sql)
  - Brings the actors from Patstat (patstat2021_staging.applt_addr_ifris), only linked to patents in the table built in the previous step (`rpd_appln_perimeter`).
  - Filters out the individuals (psn_sector = "INDIVIDUAL" in `patstat2021_staging.tls206_person`).
  - Adds some info (psn_id, psn_name, psn_sector) directly from `patstat2021_staging.tls206_person`.
  - Adds `risis_register_id` and `risis_dataset` field values from the previous table version (`risis_patents_17_2_0` schema).
  - Adds longitude, latitude info from the addresses dictionary built in the previous version (`risis_patents_17_2_0_lab.addr_dict_rpd`).
  - Adds calculation of fractional count.
  - Add NUTS and URA info from the previous version.

### [Inventors table](03_create_inventors_table.sql)
  - Brings the inventors from Patstat (patstat2021_staging.invt_addr_ifris), only linked to patents in the table built in the previous step (`rpd_appln_perimeter`).
  - Add longitude, latitude info from the addresses dictionary built in the previous version (`risis_patents_17_2_0_lab.addr_dict_rpd`).
  - Add calculation of fractional count.
  - Add NUTS and URA info from the previous version..

### [Priority attributes table](04_build_priority_attributes_table.sql)

### [Technological classification table](05_build_technological_classification_table.sql)

### [Textual information table](06_build_textual_information_table.sql)

### [Patent value table](07_build_patent_value_table.sql)