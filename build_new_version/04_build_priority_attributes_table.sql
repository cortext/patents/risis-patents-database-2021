-- Slightly adapted from the scripts applied in RPD version 2017
USE risis_patents_2021_staging;

DROP TABLE IF EXISTS rpd_priority_attributes;
-- Query OK, 0 rows affected (0.54 sec)

CREATE TABLE `rpd_priority_attributes` (
  `appln_id` int(10) NOT NULL DEFAULT '0',
  `appln_kind` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `appln_nr` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `appln_auth` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `earliest_filing_year` smallint(6) DEFAULT NULL,
  `earliest_publn_auth` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `earliest_publn_nr` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `earliest_publn_year` smallint(6) DEFAULT NULL,
  `grant_year` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`appln_id`),
  KEY `appln_kind_idx` (`appln_kind`) USING BTREE,
  KEY `appln_nr_idx` (`appln_nr`) USING BTREE,
  KEY `appln_auth_idx` (`appln_auth`) USING BTREE,
  KEY `earliest_filing_year_idx` (`earliest_filing_year`) USING BTREE,
  KEY `earliest_publn_auth_idx` (`earliest_publn_auth`) USING BTREE,
  KEY `earliest_publn_year_idx` (`earliest_publn_year`) USING BTREE,
  KEY `grant_year_idx` (`grant_year`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
-- Query OK, 0 rows affected (0.01 sec)

INSERT INTO rpd_priority_attributes (
	appln_id,
	appln_kind,
	appln_nr,
	appln_auth,
	earliest_filing_year,
	earliest_publn_year
)
SELECT
	application.appln_id,
	application.appln_kind,
	application.appln_nr,
	application.appln_auth,
	application.earliest_filing_year,
	application.earliest_publn_year
FROM risis_patents_2021_lab.rpd_appln_perimeter application; 
-- Query OK, 20656894 rows affected (14 min 51.14 sec)
-- Records: 20656894  Duplicates: 0  Warnings: 0

UPDATE
	rpd_priority_attributes small_application
	INNER JOIN
	risis_patents_2021_lab.rpd_appln_perimeter big_application
		on small_application.appln_id = big_application.appln_id
	INNER JOIN
	patstat2021_staging.tls211_pat_publn_ifris publication
		ON big_application.earliest_pat_publn_id = publication.pat_publn_id
SET
	small_application.earliest_publn_auth = publication.publn_auth,
	small_application.earliest_publn_nr = publication.publn_nr;
-- Query OK, 20656894 rows affected (1 hour 14 min 5.52 sec)
-- Rows matched: 20656894  Changed: 20656894  Warnings: 0

-- earliest publn year grant
UPDATE
	rpd_priority_attributes application
	INNER JOIN
	patstat2021_staging.tls211_pat_publn_ifris publication
	ON application.appln_id = publication.appln_id
SET
	application.grant_year = publication.publn_year
WHERE
	publication.publn_first_grant = 'Y'; 
-- Query OK, 7626207 rows affected (38 min 59.24 sec)
-- Rows matched: 7626207  Changed: 7626207  Warnings: 0
