-- Build new actors table 
USE risis_patents_2021_staging;

DROP TABLE IF EXISTS rpd_actors;
-- Query OK, 0 rows affected (1.78 sec)

CREATE TABLE `rpd_actors` (
    `appln_id` int(10) NOT NULL DEFAULT '0',
    `person_id` int(10) NOT NULL DEFAULT '0',
    `person_name` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
    `doc_std_name_id` int(10) NOT NULL DEFAULT '0',
    `doc_std_name` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
    `psn_id` int(11) DEFAULT NULL,
    `psn_name` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
    `psn_sector` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
    `name_final` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
    `adr_final` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
    `iso_ctry` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
    `continent_final` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
    `latitude` double DEFAULT NULL,
    `longitude` double DEFAULT NULL,
    `confidence` float DEFAULT NULL,
    `iso2` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
    `ura_uniqueid` int(8) DEFAULT NULL,
    `rurban_area_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
    `rurban_area_name` varchar(75) COLLATE utf8_unicode_ci DEFAULT NULL,
    `rurban_area_characteristics` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
    `nuts_source` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
    `nuts_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
    `frac_actor` float DEFAULT NULL,
    `adr_source` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
    `risis_register_id` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
    `risis_dataset` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
    PRIMARY KEY (`appln_id`, `person_id`),
    KEY `adr_final_idx` (`adr_final`(300)),
    KEY `iso_ctry_idx` (`iso_ctry`),
    KEY `adr_final_iso_ctry_idx` (`adr_final`(300), `iso_ctry`),
    KEY `appln_id_idx` (`appln_id`) USING BTREE,
    KEY `doc_std_name_id_idx` (`doc_std_name_id`) USING BTREE,
    KEY `risis_register_id_idx` (`risis_register_id`) USING BTREE,
    KEY `person_id_idx` (`person_id`) USING BTREE,
    KEY `person_name_idx` (`person_name`) USING BTREE,
    KEY `doc_std_name_idx` (`doc_std_name`(300)) USING BTREE,
    KEY `psn_id_idx` (`psn_id`) USING BTREE,
    KEY `psn_sector_idx` (`psn_sector`) USING BTREE,
    KEY `name_final_idx` (`name_final`) USING BTREE,
    KEY `continent_final_idx` (`continent_final`) USING BTREE,
    KEY `confidence_idx` (`confidence`) USING BTREE,
    KEY `rurban_area_id_idx` (`rurban_area_id`) USING BTREE,
    KEY `rurban_area_name_idx` (`rurban_area_name`) USING BTREE,
    KEY `rurban_area_characteristics_idx` (`rurban_area_characteristics`) USING BTREE,
    KEY `nuts_source_idx` (`nuts_source`) USING BTREE,
    KEY `nuts_id_idx` (`nuts_id`) USING BTREE,
    KEY `frac_actor_idx` (`frac_actor`) USING BTREE,
    KEY `lon_lat_idx` (`longitude`, `latitude`) USING BTREE,
    KEY `adr_source_idx` (`adr_source`),
    KEY `ura_uniqueid` (`ura_uniqueid`),
    KEY `iso2_idx` (`iso2`)
) ENGINE = MYISAM DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Query OK, 0 rows affected (0.01 sec)

ALTER TABLE rpd_actors DISABLE KEYS;
-- Query OK, 0 rows affected (0,00 sec)

-- Legal applicants within perimeter
INSERT INTO
    rpd_actors(
        appln_id,
        person_id,
        person_name,
        doc_std_name,
        doc_std_name_id,
        adr_final,
        name_final,
        iso_ctry,
        adr_source
    )
SELECT
    applicant.appln_id,
    applicant.person_id,
    applicant.person_name,
    applicant.doc_std_name,
    applicant.doc_std_name_id,
    applicant.adr_final,
    applicant.name_final,
    applicant.iso_ctry,
    applicant.source
FROM
    patstat2021_staging.applt_addr_ifris applicant
    INNER JOIN risis_patents_2021_lab.rpd_appln_perimeter patent_appln ON patent_appln.appln_id = applicant.appln_id
WHERE applicant.type != 'person'; -- filtering out the individuals
-- Query OK, 22568179 rows affected (10 min 13,10 sec)
-- Records: 22568179  Duplicates: 0  Warnings: 0

ALTER TABLE rpd_actors ENABLE KEYS;
-- Query OK, 0 rows affected (35 min 11,28 sec)

-- Discarding all the patents without any legal applicant
DELETE p FROM risis_patents_2021_lab.rpd_appln_perimeter p 
WHERE NOT EXISTS (
  SELECT 1
  FROM risis_patents_2021_staging.rpd_actors a 
  WHERE p.appln_id = a.appln_id
); 
-- Query OK, 421079 rows affected (6 min 59,05 sec)

-- Check address sources
SELECT adr_source, COUNT(*)
FROM risis_patents_2021_staging.rpd_actors
GROUP BY adr_source;
-- +------------+----------+
-- | adr_source | COUNT(*) |
-- +------------+----------+
-- | ARTIF      |  2460412 |
-- | IFRIS      |       44 |
-- | INPI       |    29945 |
-- | JP         |   505683 |
-- | MISSING    |  6228547 |
-- | PATSTAT    |   761593 |
-- | PROPAG     |  9173142 |
-- | REGPAT     |   105242 |
-- | STATDS     |   369424 |
-- | STATPER    |   364372 |
-- | STATPSN    |  2569775 |
-- +------------+----------+
-- 11 rows in set (12,12 sec)

-- To compare: address sources from rpd_actors table version 2017
SELECT adr_source, COUNT(*)
FROM risis_patents_17_2_0.rpd_actors
GROUP BY adr_source;
-- +-------------+----------+
-- | adr_source  | COUNT(*) |
-- +-------------+----------+
-- | NULL        | 10025081 |
-- |             |  2665160 |
-- | ARTIF       |     6700 |
-- | IFRIS       |        2 |
-- | INPI        |        3 |
-- | JP          |        4 |
-- | PATSTAT     |     5994 |
-- | PROPAG      |   415030 |
-- | PROPAGATION |   225326 |
-- | REGPAT      |     1263 |
-- | STATDS      |   115506 |
-- | STATPER     |    77137 |
-- | STATPSN     |   471695 |
-- +-------------+----------+
-- 13 rows in set (3,76 sec)

-- Add info from tls206: psn_id, psn_name, psn_sector
UPDATE
    rpd_actors actor
    INNER JOIN patstat2021_staging.tls206_person person ON actor.person_id = person.person_id
SET
actor.psn_id = person.psn_id,
actor.psn_name = person.psn_name,
actor.psn_sector = person.psn_sector;
-- Query OK, 22568179 rows affected (1 hour 33 min 47.20 sec)
-- Rows matched: 22568179  Changed: 22568179  Warnings: 0

-- Just to know
SELECT
    *
FROM
    rpd_actors
WHERE
    psn_id IS NULL
    AND iso_ctry <> "JP";
-- Empty set (0.03 sec)

-- Add risis_register_id and risis_dataset field values from the previous version table
UPDATE
    rpd_actors a
    LEFT JOIN risis_patents_17_2_0.rpd_actors b ON (
        a.appln_id = b.appln_id
        AND a.person_id = b.person_id
    )
SET
    a.risis_register_id = b.risis_register_id,
    a.risis_dataset = b.risis_dataset;
-- Query OK, 5716077 rows affected (30 min 46.84 sec)
-- Rows matched: 22568179  Changed: 5716077  Warnings: 0

-- Add calculation of fractional count
UPDATE
    rpd_actors actor
    INNER JOIN (
        SELECT
            appln_id,
            1 / count(*) AS fcount
        FROM
            rpd_actors
        GROUP BY
            appln_id
    ) frac ON actor.appln_id = frac.appln_id
SET
    actor.frac_actor = frac.fcount;
-- Query OK, 22568179 rows affected, 65535 warnings (40 min 2.92 sec)
-- Rows matched: 22568179  Changed: 22568179  Warnings: 197610
-- Warnings due to some float numbers with more than 3 decimal digits

-- Add longitude, latitude info from the addresses dictionary built in patstat2021_lab
UPDATE
    rpd_actors actor
    LEFT JOIN patstat2021_lab.addr_ifris addr_dict ON (
        actor.adr_final = addr_dict.addr_final
        AND actor.iso_ctry = addr_dict.iso_ctry
    )
SET
    actor.latitude = addr_dict.latitude,
    actor.longitude = addr_dict.longitude,
    actor.confidence = addr_dict.confidence,
    actor.iso2 = addr_dict.iso2
WHERE actor.adr_final != '' AND actor.iso_ctry != '';
-- Query OK, 14331692 rows affected (1 hour 8 min 7.12 sec)
-- Rows matched: 16195672  Changed: 14331692  Warnings: 0

CREATE INDEX idx_ura_uniqueid USING BTREE ON rpd_actors (ura_uniqueid);
-- Query OK, 22568179 rows affected (53 min 37.80 sec)
-- Records: 22568179  Duplicates: 0  Warnings: 0

-- Waiting for geomapping results
-- Add NUTS and URA info from the previous version
UPDATE
    rpd_actors a
    LEFT JOIN patstat2021_lab.addr_ifris_geo_mapping b ON (
      a.longitude = b.longitude 
      AND a.latitude = b.latitude
    )
SET
    a.ura_uniqueid = b.ura_uniqueid,
    a.rurban_area_id = b.rurban_area_id,
    a.rurban_area_name = b.rurban_area_name,
    a.rurban_area_characteristics = b.rurban_area_characteristics,
    a.nuts_source = b.nuts_source,
    a.nuts_id = b.nuts_id
WHERE a.longitude IS NOT NULL AND a.ura_uniqueid IS NULL;
-- Query OK, 14248897 rows affected (2 hours 12 min 57.24 sec)
-- Rows matched: 14331692  Changed: 14248897  Warnings: 0
