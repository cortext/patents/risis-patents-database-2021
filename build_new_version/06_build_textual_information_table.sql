-- Slightly adapted from the scripts applied in RPD version 2017
USE risis_patents_2021_staging;

DROP TABLE IF EXISTS rpd_textual_information;
-- Query OK, 0 rows affected, 1 warning (0.00 sec)

CREATE TABLE `rpd_textual_information` (
  `appln_id` int(10) NOT NULL DEFAULT '0',
  `appln_title` text COLLATE utf8_unicode_ci,
  `appln_title_lg` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `appln_title_en` text COLLATE utf8_unicode_ci,
  `appln_title_en_source` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `appln_abstract` text COLLATE utf8_unicode_ci,
  `appln_abstract_lg` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `appln_abstract_en` text COLLATE utf8_unicode_ci,
  `appln_abstract_en_source` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ipc_description` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`appln_id`),
  KEY `appln_title_lg_idx` (`appln_title_lg`) USING BTREE,
  KEY `appln_title_en_source_idx` (`appln_title_en_source`) USING BTREE,
  KEY `appln_abstract_lg_idx` (`appln_abstract_lg`) USING BTREE,
  KEY `appln_abstract_en_source_idx` (`appln_abstract_en_source`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
-- Query OK, 0 rows affected (0.01 sec)

INSERT INTO
  rpd_textual_information(appln_id)
SELECT
  appln_id
FROM
  risis_patents_2021_lab.rpd_appln_perimeter;
-- Query OK, 20656894 rows affected (8 min 3.30 sec)
-- Records: 20656894  Duplicates: 0  Warnings: 0

UPDATE
  rpd_textual_information txt
  INNER JOIN patstat2021_staging.tls202_appln_title_ifris title ON txt.appln_id = title.appln_id
SET
  txt.appln_title = title.appln_title,
  txt.appln_title_en = title.appln_title_en,
  txt.appln_title_lg = title.appln_title_lg,
  txt.appln_title_en_source = title.appln_title_en_source;
-- Query OK, 20654533 rows affected (1 hour 26 min 8.80 sec)
-- Rows matched: 20654533  Changed: 20654533  Warnings: 0

UPDATE
  rpd_textual_information txt
  INNER JOIN patstat2021_staging.tls203_appln_abstr_ifris abstract ON txt.appln_id = abstract.appln_id
SET
  txt.appln_abstract = abstract.appln_abstract,
  txt.appln_abstract_en = abstract.appln_abstract_en,
  txt.appln_abstract_lg = abstract.appln_abstract_lg,
  txt.appln_abstract_en_source = abstract.appln_abstract_en_source;
-- Query OK, 19929356 rows affected (1 hour 31 min 44.26 sec)
-- Rows matched: 19929356  Changed: 19929356  Warnings: 0

UPDATE
  rpd_textual_information txt
  INNER JOIN patstat2021_staging.ipc_technology_frac_ifris a ON txt.appln_id = a.appln_id
  INNER JOIN patstat2021_staging.ipc_description b ON a.ipc_class_symbol = b.ipc_class_level
SET
  txt.ipc_description = b.ipc_description;
-- Query OK, 20144081 rows affected (1 hour 16 min 7.11 sec)
-- Rows matched: 20277718  Changed: 20144081  Warnings: 0
