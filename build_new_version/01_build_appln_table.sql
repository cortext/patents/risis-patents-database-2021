-- Build new patent applications table 
USE risis_patents_2021_lab;

DROP TABLE IF EXISTS rpd_appln_perimeter;

CREATE TABLE rpd_appln_perimeter LIKE patstat2021_staging.tls201_appln_ifris;

INSERT INTO
    rpd_appln_perimeter
SELECT
    *
FROM
    patstat2021_staging.tls201_appln_ifris a
WHERE
    a.appln_filing_year >= 2000
    AND a.ipr_type = 'PI'
    AND a.appln_kind IN ('A', 'W', 'P')
    AND a.appln_id = a.earliest_filing_id
    AND a.appln_filing_year <> 9999
    AND a.earliest_filing_year <> 9999
;
-- Query OK, 21077973 rows affected (20 min 46,91 sec)
-- Records: 21077973  Duplicates: 0  Warnings: 0
