-- Slightly adapted from the scripts applied in RPD version 2017
USE risis_patents_2021_staging;

DROP TABLE IF EXISTS rpd_patent_value;
-- Query OK, 0 rows affected (0.01 sec)

CREATE TABLE `rpd_patent_value` (
  `appln_id` int(10) NOT NULL DEFAULT '0',
  `granted` int(11) DEFAULT NULL,
  `nb_citing_docdb_fam` smallint(6) DEFAULT NULL,
  `singleton` int(1) NOT NULL DEFAULT '0',
  `transnat` int(1) NOT NULL DEFAULT '0',
  `ip5_family` int(1) NOT NULL DEFAULT '0',
  `triadic` int(1) NOT NULL DEFAULT '0',
  `ip5` int(1) NOT NULL DEFAULT '0',
  `_2ip5` int(1) NOT NULL DEFAULT '0',
  `US` int(1) NOT NULL DEFAULT '0',
  `EP` int(1) NOT NULL DEFAULT '0',
  `JP` int(1) NOT NULL DEFAULT '0',
  `CN` int(1) NOT NULL DEFAULT '0',
  `KR` int(1) NOT NULL DEFAULT '0',
  `PCT_in_inpadoc_family` int(1) NOT NULL DEFAULT '0',
  `nb_patents_docdb` smallint(6) DEFAULT NULL,
  `nb_offices_docdb` smallint(6) DEFAULT NULL,
  `nb_patents_inpadoc` smallint(6) DEFAULT NULL,
  `nb_offices_inpadoc` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`appln_id`),
  KEY `granted_idx` (`granted`) USING BTREE,
  KEY `nb_citing_docdb_fam_idx` (`nb_citing_docdb_fam`) USING BTREE,
  KEY `singleton_idx` (`singleton`) USING BTREE,
  KEY `transnat_idx` (`transnat`) USING BTREE,
  KEY `ip5_family_idx` (`ip5_family`) USING BTREE,
  KEY `triadic_idx` (`triadic`) USING BTREE,
  KEY `ip5_idx` (`ip5`) USING BTREE,
  KEY `US_idx` (`US`) USING BTREE,
  KEY `EP_idx` (`EP`) USING BTREE,
  KEY `JP_idx` (`JP`) USING BTREE,
  KEY `CN_idx` (`CN`) USING BTREE,
  KEY `KR_idx` (`KR`) USING BTREE,
  KEY `PCT_in_inpadoc_family_idx` (`PCT_in_inpadoc_family`) USING BTREE,
  KEY `nb_patents_docdb_idx` (`nb_patents_docdb`) USING BTREE,
  KEY `nb_offices_docdb_idx` (`nb_offices_docdb`) USING BTREE,
  KEY `nb_patents_inpadoc_idx` (`nb_patents_inpadoc`) USING BTREE,
  KEY `nb_offices_inpadoc_idx` (`nb_offices_inpadoc`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
-- Query OK, 0 rows affected (0.00 sec)

INSERT INTO
  rpd_patent_value (
    appln_id,
    granted,
    nb_citing_docdb_fam,
    singleton,
    transnat
  )
SELECT
  appln_id,
  CASE
    WHEN granted = 'Y' THEN 1
    WHEN granted = 'N' THEN 0
    ELSE NULL
  END,
  nb_citing_docdb_fam,
  singleton,
  transnat
FROM
  risis_patents_2021_lab.rpd_appln_perimeter;
-- Query OK, 20656894 rows affected (10 min 47.69 sec)
-- Records: 20656894  Duplicates: 0  Warnings: 0

/* Temporary table to see whether an inpadoc family has members with authority = ('US', 'EP', 'JP', 'CN', 'KR') */
DROP TABLE IF EXISTS risis_patents_2021_lab.tmp_inpadoc_auth;
-- Query OK, 0 rows affected, 1 warning (0.01 sec)

CREATE TABLE risis_patents_2021_lab.tmp_inpadoc_auth(
  `inpadoc_family_id` int(11) DEFAULT NULL,
  `US` int(10) NOT NULL DEFAULT 0,
  `EP` int(10) NOT NULL DEFAULT 0,
  `JP` int(10) NOT NULL DEFAULT 0,
  `CN` int(10) NOT NULL DEFAULT 0,
  `KR` int(10) NOT NULL DEFAULT 0,
  `triadic` int(1) NOT NULL DEFAULT 0,
  `ip5` int(1) NOT NULL DEFAULT 0
);
-- Query OK, 0 rows affected (0.00 sec)

INSERT INTO
  risis_patents_2021_lab.tmp_inpadoc_auth (inpadoc_family_id, US, EP, JP, CN, KR)
SELECT
  inpadoc_family_id,
  sum(
    CASE
      WHEN appln_auth = "US" THEN 1
      ELSE 0
    END
  ) AS us_count,
  sum(
    CASE
      WHEN appln_auth = "EP" THEN 1
      ELSE 0
    END
  ) AS ep_count,
  sum(
    CASE
      WHEN appln_auth = "JP" THEN 1
      ELSE 0
    END
  ) AS jp_count,
  sum(
    CASE
      WHEN appln_auth = "CN" THEN 1
      ELSE 0
    END
  ) AS cn_count,
  sum(
    CASE
      WHEN appln_auth = "KR" THEN 1
      ELSE 0
    END
  ) AS kr_count
FROM
  patstat2021_staging.tls201_appln_ifris
GROUP BY
  inpadoc_family_id;
-- Query OK, 50546727 rows affected (2 min 16.46 sec)
-- Records: 50546727  Duplicates: 0  Warnings: 0

CREATE INDEX inpadoc_family_id_idx USING btree ON risis_patents_2021_lab.tmp_inpadoc_auth(inpadoc_family_id);
-- Query OK, 50546727 rows affected (36.58 sec)
-- Records: 50546727  Duplicates: 0  Warnings: 0

/* ip5 family: transnat with at least one deposit in an IP5 in the family*/
/* 	ip5_family = 1 if: */
/*		patent.transnat = 1 AND ( */
/*			patent not on ip5 office AND us + ep + jp + cn + kr > 0 */
/*			OR */
/*			patent on ip5 office 	 AND us + ep + jp + cn + kr > 1 */
/*		) */
UPDATE
  rpd_patent_value value
  INNER JOIN risis_patents_2021_lab.rpd_appln_perimeter application ON value.appln_id = application.appln_id
  INNER JOIN risis_patents_2021_lab.tmp_inpadoc_auth auth_count ON application.inpadoc_family_id = auth_count.inpadoc_family_id
SET
  value.ip5_family = 1
WHERE
  value.transnat = 1
  AND auth_count.us + auth_count.ep + auth_count.jp + auth_count.cn + auth_count.kr > 1;
-- Query OK, 3302402 rows affected (5 min 59.10 sec)
-- Rows matched: 3302402  Changed: 3302402  Warnings: 0

/* ip5 and triadic */
UPDATE
  risis_patents_2021_lab.tmp_inpadoc_auth
SET
  us = 1
WHERE
  us > 0;
-- Query OK, 2239108 rows affected (19.38 sec)
-- Rows matched: 10007461  Changed: 2239108  Warnings: 0

UPDATE
  risis_patents_2021_lab.tmp_inpadoc_auth
SET
  ep = 1
WHERE
  ep > 0;
-- Query OK, 325578 rows affected (6.34 sec)
-- Rows matched: 3248826  Changed: 325578  Warnings: 0

UPDATE
  risis_patents_2021_lab.tmp_inpadoc_auth
SET
  jp = 1
WHERE
  jp > 0;
-- Query OK, 672459 rows affected (9.70 sec)
-- Rows matched: 12115093  Changed: 672459  Warnings: 0

UPDATE
  risis_patents_2021_lab.tmp_inpadoc_auth
SET
  cn = 1
WHERE
  cn > 0;
-- Query OK, 291515 rows affected (7.00 sec)
-- Rows matched: 19681331  Changed: 291515  Warnings: 0

UPDATE
  risis_patents_2021_lab.tmp_inpadoc_auth
SET
  kr = 1
WHERE
  kr > 0;
-- Query OK, 141052 rows affected (5.34 sec)
-- Rows matched: 2851437  Changed: 141052  Warnings: 0

UPDATE
  risis_patents_2021_lab.tmp_inpadoc_auth
SET
  triadic = 1
WHERE
  us > 0
  AND ep > 0
  AND jp > 0;
-- Query OK, 1002008 rows affected (10.19 sec)
-- Rows matched: 1002008  Changed: 1002008  Warnings: 0

UPDATE
  risis_patents_2021_lab.tmp_inpadoc_auth
SET
  ip5 = 1
WHERE
  (us + ep + jp + cn + kr) = 5;
-- Query OK, 214476 rows affected (9.70 sec)
-- Rows matched: 214476  Changed: 214476  Warnings: 0

UPDATE
  rpd_patent_value value
  INNER JOIN risis_patents_2021_lab.rpd_appln_perimeter application ON value.appln_id = application.appln_id
  INNER JOIN risis_patents_2021_lab.tmp_inpadoc_auth auth ON application.inpadoc_family_id = auth.inpadoc_family_id
SET
  value.US = auth.US,
  value.EP = auth.EP,
  value.JP = auth.JP,
  value.CN = auth.CN,
  value.KR = auth.KR,
  value.ip5 = auth.ip5,
  value.triadic = auth.triadic;
-- Query OK, 18943657 rows affected (33 min 57.38 sec)
-- Rows matched: 20656894  Changed: 18943657  Warnings: 0

DROP TABLE risis_patents_2021_lab.tmp_inpadoc_auth;
-- Query OK, 0 rows affected (0.49 sec)

/* PCT_in_inpadoc_family: 1 when appln_kind = 'W' */
DROP TABLE IF EXISTS risis_patents_2021_lab.tmp_inpadoc_pct;
-- Query OK, 0 rows affected, 1 warning (0.00 sec)

CREATE TABLE risis_patents_2021_lab.tmp_inpadoc_pct(
  `inpadoc_family_id` int(11) DEFAULT NULL,
  `pct_count` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY(inpadoc_family_id)
);
-- Query OK, 0 rows affected (0.01 sec)

INSERT INTO
  risis_patents_2021_lab.tmp_inpadoc_pct
SELECT
  inpadoc_family_id,
  sum(
    CASE
      WHEN appln_kind = 'W' THEN 1
      ELSE 0
    END
  ) pct_count
FROM
  patstat2021_staging.tls201_appln_ifris
GROUP BY
  inpadoc_family_id;
-- Query OK, 50546727 rows affected (14 min 52.20 sec)
-- Records: 50546727  Duplicates: 0  Warnings: 0

UPDATE
  rpd_patent_value value
  INNER JOIN risis_patents_2021_lab.rpd_appln_perimeter application ON value.appln_id = application.appln_id
  INNER JOIN risis_patents_2021_lab.tmp_inpadoc_pct pct ON application.inpadoc_family_id = pct.inpadoc_family_id
SET
  value.PCT_in_inpadoc_family = 1
WHERE
  pct.pct_count > 0;
-- Query OK, 3312187 rows affected (14 min 25.26 sec)
-- Rows matched: 3312187  Changed: 3312187  Warnings: 0

DROP TABLE risis_patents_2021_lab.tmp_inpadoc_pct;
-- Query OK, 0 rows affected (0.39 sec)

-- ---------------------------------------------------------------------------------------
-- nb_patents_inpadoc: inpadoc_family_size
-- nb_offices_inpadoc: distinct authority group by inpadoc_family 
-- ---------------------------------------------------------------------------------------
-- Create temporal tables to calculate inpadoc family size and number of offices
CREATE TABLE risis_patents_2021_lab.tmp_inpadoc_family_size 
SELECT
  inpadoc_family_id,
  count(*) AS inpadoc_family_size
FROM
  patstat2021_staging.tls201_appln_ifris
GROUP BY
  inpadoc_family_id;
-- Query OK, 50546727 rows affected (1 min 0.00 sec)
-- Records: 50546727  Duplicates: 0  Warnings: 0

CREATE INDEX inpadoc_family_id_idx USING btree 
  ON risis_patents_2021_lab.tmp_inpadoc_family_size(inpadoc_family_id);
-- Query OK, 50546727 rows affected (31,97 sec)
-- Records: 50546727  Duplicates: 0  Warnings: 0

CREATE TABLE risis_patents_2021_lab.tmp_inpadoc_nb_offices
SELECT
  inpadoc_family_id,
  count(DISTINCT appln_auth) AS nb_offices_inpadoc
FROM
  patstat2021_staging.tls201_appln_ifris
GROUP BY
  inpadoc_family_id;
-- Query OK, 50546727 rows affected (3 min 25.36 sec)
-- Records: 50546727  Duplicates: 0  Warnings: 0

CREATE INDEX inpadoc_family_id_idx USING btree 
  ON risis_patents_2021_lab.tmp_inpadoc_nb_offices(inpadoc_family_id);
-- Query OK, 50546727 rows affected (30.90 sec)
-- Records: 50546727  Duplicates: 0  Warnings: 0

-- Update inpadoc family size and number of offices based on the previous calculations
UPDATE
  rpd_patent_value value
  INNER JOIN risis_patents_2021_lab.rpd_appln_perimeter application 
    ON value.appln_id = application.appln_id
  INNER JOIN risis_patents_2021_lab.tmp_inpadoc_family_size inpadoc_family 
    ON application.inpadoc_family_id = inpadoc_family.inpadoc_family_id
  INNER JOIN risis_patents_2021_lab.tmp_inpadoc_nb_offices inpadoc_offices 
    ON application.inpadoc_family_id = inpadoc_offices.inpadoc_family_id
SET
  value.nb_patents_inpadoc = inpadoc_family.inpadoc_family_size,
  value.nb_offices_inpadoc = inpadoc_offices.nb_offices_inpadoc;
-- Query OK, 20656894 rows affected (48 min 54.58 sec)
-- Rows matched: 20656894  Changed: 20656894  Warnings: 0

-- ---------------------------------------------------------------------------------------
-- nb_patents_docdb: docdb_family_size
-- nb_offices_docdb: distinct authority group by docdb_family
-- ---------------------------------------------------------------------------------------
-- Create temporal tables to calculate docdb family size and number of offices

CREATE TABLE risis_patents_2021_lab.tmp_docdb_family_size 
SELECT
  docdb_family_id,
  max(docdb_family_size) AS docdb_family_size
FROM
  patstat2021_staging.tls201_appln_ifris
GROUP BY
  docdb_family_id;
-- Query OK, 58322584 rows affected (3 min 35.12 sec)
-- Records: 58322584  Duplicates: 0  Warnings: 0

CREATE INDEX docdb_family_id_idx USING btree 
  ON risis_patents_2021_lab.tmp_docdb_family_size(docdb_family_id);
-- Query OK, 58322584 rows affected (35.46 sec)
-- Records: 58322584  Duplicates: 0  Warnings: 0

CREATE INDEX idx_docdb_family_id USING btree 
  ON patstat2021_staging.tls201_appln_ifris(docdb_family_id);
-- Query OK, 84464795 rows affected (1 hour 20 min 17,33 sec)
-- Records: 84464795  Duplicates: 0  Warnings: 0

CREATE INDEX idx_docdb_family_id_appln_auth USING BTREE 
  ON patstat2021_staging.tls201_appln_ifris (docdb_family_id,appln_auth);
-- Query OK, 84464795 rows affected (1 hour 24 min 24,01 sec)
-- Records: 84464795  Duplicates: 0  Warnings: 0

CREATE TABLE risis_patents_2021_lab.tmp_docdb_nb_offices
SELECT
  docdb_family_id,
  count(DISTINCT appln_auth) AS nb_offices_docdb
FROM
  patstat2021_staging.tls201_appln_ifris
GROUP BY
  docdb_family_id;
-- Query OK, 58322584 rows affected (1 min 17,44 sec)
-- Records: 58322584  Duplicates: 0  Warnings: 0

CREATE INDEX docdb_family_id_idx USING btree 
  ON risis_patents_2021_lab.tmp_docdb_nb_offices(docdb_family_id);
-- Query OK, 58322584 rows affected (35,75 sec)
-- Records: 58322584  Duplicates: 0  Warnings: 0

-- Update docdb family size and number of offices based on the previous calculations
UPDATE
  rpd_patent_value value
  INNER JOIN risis_patents_2021_lab.rpd_appln_perimeter application 
    ON value.appln_id = application.appln_id
  INNER JOIN risis_patents_2021_lab.tmp_docdb_family_size docdb_family 
    ON application.docdb_family_id = docdb_family.docdb_family_id
  INNER JOIN risis_patents_2021_lab.tmp_docdb_nb_offices docdb_offices 
    ON application.docdb_family_id = docdb_offices.docdb_family_id
SET
  value.nb_patents_docdb = docdb_family.docdb_family_size,
  value.nb_offices_docdb = docdb_offices.nb_offices_docdb;
-- Query OK, 20656894 rows affected (25 min 44,61 sec)
-- Rows matched: 20656894  Changed: 20656894  Warnings: 0
