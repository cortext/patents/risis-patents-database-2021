-- Slightly adapted from the scripts applied in RPD version 2017
USE risis_patents_2021_staging;

DROP TABLE IF EXISTS rpd_technological_classification;
-- Query OK, 0 rows affected, 1 warning (0.00 sec)

CREATE TABLE `rpd_technological_classification` (
  `appln_id` int(10) NOT NULL DEFAULT '0',
  `ipc_class_symbol` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `frac_ipc` decimal(5,4) DEFAULT NULL,
  `domains` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fields` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subfields` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ipc_class_level` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`appln_id`,`ipc_class_symbol`),
  KEY `appln_id_idx` (`appln_id`) USING BTREE,
  KEY `ipc_class_symbol_idx` (`ipc_class_symbol`) USING BTREE,
  KEY `frac_ipc_idx` (`frac_ipc`) USING BTREE,
  KEY `domains_idx` (`domains`) USING BTREE,
  KEY `fields_idx` (`fields`) USING BTREE,
  KEY `subfields_idx` (`subfields`) USING BTREE,
  KEY `ipc_class_level_idx` (`ipc_class_level`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
-- Query OK, 0 rows affected (0.01 sec)

INSERT INTO
  rpd_technological_classification(
    appln_id,
    ipc_class_symbol,
    frac_ipc,
    domains,
    `fields`,
    subfields,
    ipc_class_level
  )
SELECT
  application.appln_id,
  technology.ipc_class_symbol,
  technology.frac_ipc,
  technology.domaines domains,
  technology.fields,
  technology.sfields subfields,
  technology.ipc_class_level
FROM
  risis_patents_2021_lab.rpd_appln_perimeter application
  INNER JOIN patstat2021_staging.ipc_technology_frac_ifris technology ON application.appln_id = technology.appln_id;
-- Query OK, 58804440 rows affected (1 hour 35 min 37.28 sec)
-- Records: 58804440  Duplicates: 0  Warnings: 0
