-- Set to null all the OrgReg values
USE risis_patents_2021_staging;

UPDATE
  rpd_actors
SET
  risis_dataset = NULL,
  risis_register_id = NULL
WHERE
  risis_dataset = 'ORGREG';
-- Query OK, 105684 rows affected (6 min 18,55 sec)
-- Rows matched: 105684  Changed: 105684  Warnings: 0

SELECT
  COUNT(*)
FROM
  rpd_actors a
  INNER JOIN risis_patents_17_2_0_lab.rpd_orgreg_matching b ON a.person_id = b.person_id;
-- 181.491

-- Update OrgReg id values only for the empty ones.
UPDATE
  rpd_actors a
  INNER JOIN risis_patents_17_2_0_lab.rpd_orgreg_matching b ON a.person_id = b.person_id
SET
  a.risis_dataset = 'ORGREG',
  a.risis_register_id = b.risis_register_id;
-- Query OK, 178833 rows affected (21,69 sec)
-- Rows matched: 178833  Changed: 178833  Warnings: 0
