# RISIS Patents database - RPD 2021

RISIS patents database is the publicly available compilation of our team's work with patents.
The goal of the SQL scripts in this repo is to make it easier to go from [Patstat](https://gitlab.com/cortext/patents/patstat-2021/) and our iterative enrichments to a stable version of RISIS patents database.
Scripts are grouped, named and organised according to their purpose and order in their respective sequence of steps.

Further documentation can be found [here]().