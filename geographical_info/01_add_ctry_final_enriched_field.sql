USE risis_patents_2021_staging;

-- -------------------------------------------------------------------------------
-- rpd_inventors
ALTER TABLE rpd_inventors 
ADD COLUMN ctry_final_enriched CHAR(2) NULL AFTER iso_ctry;
-- Query OK, 59150156 rows affected (1 hour 35 min 4,62 sec)
-- Records: 59150156  Duplicates: 0  Warnings: 0

ALTER TABLE rpd_inventors 
ADD COLUMN source_ctry_final_enriched VARCHAR(50) NULL AFTER ctry_final_enriched;
-- Query OK, 59150156 rows affected (2 hours 12 min 42.99 sec)
-- Records: 59150156  Duplicates: 0  Warnings: 0

UPDATE rpd_inventors
SET
  ctry_final_enriched = iso_ctry,
  source_ctry_final_enriched = "INV_CTRY"
WHERE iso_ctry IS NOT NULL AND iso_ctry != '';
-- Query OK, 39958215 rows affected (24 min 11.37 sec)
-- Rows matched: 39958215  Changed: 39958215  Warnings: 0

UPDATE rpd_inventors
SET
  ctry_final_enriched = iso2,
  source_ctry_final_enriched = "GEOC_ADDR"
WHERE ctry_final_enriched IS NULL 
  AND iso2 IS NOT NULL AND iso2 != '';
-- Query OK, 18339 rows affected (14 min 5.93 sec)
-- Rows matched: 18339  Changed: 18339  Warnings: 0

-- -------------------------------------------------------------------------------
-- rpd_actors
ALTER TABLE rpd_actors 
ADD COLUMN ctry_final_enriched CHAR(2) NULL AFTER iso_ctry;
-- Query OK, 22568179 rows affected (56 min 33,32 sec)
-- Records: 22568179  Duplicates: 0  Warnings: 0

ALTER TABLE rpd_actors 
ADD COLUMN source_ctry_final_enriched VARCHAR(50) NULL AFTER ctry_final_enriched;
-- Query OK, 22568179 rows affected (51 min 24.09 sec)
-- Records: 22568179  Duplicates: 0  Warnings: 0

UPDATE rpd_actors
SET
  ctry_final_enriched = iso_ctry,
  source_ctry_final_enriched = "INV_CTRY"
WHERE iso_ctry IS NOT NULL AND iso_ctry != '';
-- Query OK, 17200599 rows affected (10 min 15.81 sec)
-- Rows matched: 17200599  Changed: 17200599  Warnings: 0

UPDATE rpd_actors
SET
  ctry_final_enriched = iso2,
  source_ctry_final_enriched = "GEOC_ADDR"
WHERE (iso_ctry = "CN" and iso2 = "HK") OR (iso_ctry = "CN" and iso2 = "TW");
-- Query OK, 3353 rows affected (5.23 sec)
-- Rows matched: 3353  Changed: 3353  Warnings: 0

UPDATE rpd_actors a
INNER JOIN patstat2021_staging.tls206_person b
ON a.person_id = b.person_id
SET
  a.ctry_final_enriched = b.person_ctry_code,
  a.source_ctry_final_enriched = "INV_CTRY_RAW_DATA"
WHERE a.iso_ctry = "JP" and a.iso2="GB" and b.person_ctry_code = "DE"
-- Query OK, 8967 rows affected (38.27 sec)
-- Rows matched: 8967  Changed: 8967  Warnings: 0

UPDATE rpd_actors
SET
  ctry_final_enriched = "BR",
  source_ctry_final_enriched = "MANUAL"
WHERE iso_ctry = "JP" and iso2 = "LV" and adr_final = "Federal Republic of Brazil";
-- Query OK, 91 rows affected (0.42 sec)
-- Rows matched: 91  Changed: 91  Warnings: 0

-- -------------------------------------------------------------------------------
-- rpd_inventors
CREATE TABLE risis_patents_2021_lab.tmp_appln_id_ctry (
  appln_id int(10) NOT NULL,
  iso_ctry char(2) CHARACTER SET utf8 NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
-- Query OK, 0 rows affected (0,07 sec)

-- Selecting the appln_id values from the inventors without a value 
-- for ctry_final_enriched
INSERT INTO risis_patents_2021_lab.tmp_appln_id_ctry (appln_id)
SELECT DISTINCT appln_id
FROM rpd_inventors 
WHERE ctry_final_enriched IS NULL;
-- Query OK, 7536984 rows affected (5 min 11,12 sec)
-- Records: 7536984  Duplicates: 0  Warnings: 0

UPDATE risis_patents_2021_lab.tmp_appln_id_ctry a 
INNER JOIN rpd_actors b 
  ON a.appln_id = b.appln_id
INNER JOIN rpd_priority_attributes c 
  ON b.appln_id = c.appln_id
SET 
  a.iso_ctry = b.ctry_final_enriched
WHERE b.ctry_final_enriched = c.appln_auth;
-- Query OK, 3401313 rows affected (5 min 31.80 sec)
-- Rows matched: 3401313  Changed: 3401313  Warnings: 0

UPDATE rpd_inventors a 
INNER JOIN risis_patents_2021_lab.tmp_appln_id_ctry b 
ON a.appln_id = b.appln_id
SET
  a.ctry_final_enriched = b.iso_ctry,
  a.source_ctry_final_enriched = "INHERITED"
WHERE a.ctry_final_enriched IS NULL AND b.iso_ctry IS NOT NULL 
  AND b.iso_ctry != '';
-- Query OK, 9341708 rows affected (7 min 55.00 sec)
-- Rows matched: 9341708  Changed: 9341708  Warnings: 0
