USE risis_patents_2021_staging;

-- -------------------------------------------------------------------------------
-- rpd_actors

SELECT DISTINCT continent FROM patstat2021_lab.ura_all_info;
-- +-------------------------+
-- | continent               |
-- +-------------------------+
-- | South America           |
-- | Asia                    |
-- |                         |
-- | Africa                  |
-- | Europe                  |
-- | North America           |
-- | Oceania                 |
-- | Antarctica              |
-- | Seven seas (open ocean) |
-- +-------------------------+

-- Update based on the ura_uniqueid value
UPDATE rpd_actors a 
  INNER JOIN patstat2021_lab.ura_all_info b 
  ON a.ura_uniqueid = b.uniqueid 
SET 
  a.continent_final = 
  CASE
    WHEN continent = "South America" THEN "SA"
    WHEN continent = "Asia" THEN "AS"
    WHEN continent = "Africa" THEN "AF"
    WHEN continent = "Europe" THEN "EU"
    WHEN continent = "North America" THEN "NA"
    WHEN continent = "Oceania" THEN "OC"
  END;
-- Query OK, 14248249 rows affected (39 min 50,49 sec)
-- Rows matched: 14248897  Changed: 14248249  Warnings: 0

-- For the rest: update based on their country (except for those countries 
-- located in two continents)

SELECT DISTINCT continent FROM patstatAvr2017.tls801_country;
-- +-----------------------+
-- | continent             |
-- +-----------------------+
-- |                       |
-- | Europe                |
-- | Asia                  |
-- | North America         |
-- | Africa                |
-- | South America         |
-- | Australia and Oceania |
-- | Europe/Asia           |
-- +-----------------------+

UPDATE rpd_actors a 
  INNER JOIN patstatAvr2017.tls801_country b 
  ON a.iso2 = b.ctry_code 
SET 
  a.continent_final = 
  CASE
    WHEN continent = "South America" THEN "SA"
    WHEN continent = "Asia" THEN "AS"
    WHEN continent = "Africa" THEN "AF"
    WHEN continent = "Europe" THEN "EU"
    WHEN continent = "North America" THEN "NA"
    WHEN continent = "Australia and Oceania" THEN "OC"
  END
WHERE a.continent_final IS NULL;
-- Query OK, 67635 rows affected (4 min 20,22 sec)
-- Rows matched: 83346  Changed: 67635  Warnings: 0

-- Update based on the new field for country: ctry_final_enriched
UPDATE rpd_actors a 
  INNER JOIN patstatAvr2017.tls801_country b 
  ON a.ctry_final_enriched = b.ctry_code 
SET 
  a.continent_final = 
  CASE
    WHEN continent = "South America" THEN "SA"
    WHEN continent = "Asia" THEN "AS"
    WHEN continent = "Africa" THEN "AF"
    WHEN continent = "Europe" THEN "EU"
    WHEN continent = "North America" THEN "NA"
    WHEN continent = "Australia and Oceania" THEN "OC"
  END
WHERE a.continent_final IS NULL;
-- Query OK, 2706941 rows affected (5 min 16,84 sec)
-- Rows matched: 2884702  Changed: 2706941  Warnings: 0

-- -------------------------------------------------------------------------------
-- rpd_inventors

-- Update based on the ura_uniqueid value
UPDATE rpd_inventors a 
  INNER JOIN patstat2021_lab.ura_all_info b 
  ON a.ura_uniqueid = b.uniqueid 
SET 
  a.continent_final = 
  CASE
    WHEN continent = "South America" THEN "SA"
    WHEN continent = "Asia" THEN "AS"
    WHEN continent = "Africa" THEN "AF"
    WHEN continent = "Europe" THEN "EU"
    WHEN continent = "North America" THEN "NA"
    WHEN continent = "Oceania" THEN "OC"
  END;
-- Query OK, 36499718 rows affected (1 hour 3 min 4,31 sec)
-- Rows matched: 36500331  Changed: 36499718  Warnings: 0

-- Update based on the ctry_final_enriched field
UPDATE rpd_inventors a 
  INNER JOIN patstatAvr2017.tls801_country b 
  ON a.ctry_final_enriched = b.ctry_code 
SET 
  a.continent_final = 
  CASE
    WHEN continent = "South America" THEN "SA"
    WHEN continent = "Asia" THEN "AS"
    WHEN continent = "Africa" THEN "AF"
    WHEN continent = "Europe" THEN "EU"
    WHEN continent = "North America" THEN "NA"
    WHEN continent = "Australia and Oceania" THEN "OC"
  END
WHERE a.continent_final IS NULL;
-- Query OK, 12769207 rows affected (22 min 19,76 sec)
-- Rows matched: 12818477  Changed: 12769207  Warnings: 0
